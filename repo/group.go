package repo

import (
	"github.com/treehouse-monitor/models"
	"gorm.io/gorm"
)

type GroupRepository interface {
	InsertGroup(group models.Group) models.Group
	UpdateGroup(group models.Group) models.Group
	DeleteGroup(group models.Group) (string, error)
	FindGroup(group models.Group) models.Group
}

type groupConnection struct {
	connection *gorm.DB
}

// DeleteGroup implements GroupRepository
func (*groupConnection) DeleteGroup(group models.Group) (string, error) {
	panic("unimplemented")
}

// FindGroup implements GroupRepository
func (*groupConnection) FindGroup(group models.Group) models.Group {
	panic("unimplemented")
}

// InsertGroup implements GroupRepository
func (*groupConnection) InsertGroup(group models.Group) models.Group {
	panic("unimplemented")
}

// UpdateGroup implements GroupRepository
func (*groupConnection) UpdateGroup(group models.Group) models.Group {
	panic("unimplemented")
}

func NewGroupRepository(connection *gorm.DB) GroupRepository {
	return &groupConnection{connection: connection}
}
