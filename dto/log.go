package dto

type UpdateLogRequest struct {
	ID      int64  `json:"id" form:"id"`
	HashID  string `json:"hash_id"  binding:"required"`
	GroupID string `json:"group_id"`
}
