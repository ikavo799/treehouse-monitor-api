package repo

import (
	"fmt"

	"github.com/treehouse-monitor/models"
	"gorm.io/gorm"
)

type LogRepository interface {
	All() ([]models.Log, error)
	InsertLog(log models.Log) (models.Log, error)
	UpdateLog(log models.Log) (models.Log, error)
	DeleteLog(log models.Log) (string, error)
	FindLogByID(logId string) (models.Log, error)
	ApplyGroup(groupIds string, log models.Log) (models.Log, error)
}
type logRepo struct {
	connection *gorm.DB
}

// ApplyGroup implements LogRepository
func (l *logRepo) ApplyGroup(groupId string, log models.Log) (models.Log, error) {
	var group models.Group
	err := l.connection.Find(&group, groupId)
	if err != nil {
		panic(err)
	}
	log.Group = group
	l.connection.Save(&log)
	return log, nil
}

// DeleteLog implements LogRepository
func (l *logRepo) DeleteLog(log models.Log) (string, error) {
	l.connection.Delete(&log)
	return fmt.Sprint(log.ID), nil

}

// FindLogByID implements LogRepository
func (l *logRepo) FindLogByID(logId string) (models.Log, error) {
	var log models.Log
	l.connection.Where("id = ?", logId).Take(&log)
	return log, nil
}

// InsertLog implements LogRepository
func (l *logRepo) InsertLog(log models.Log) (models.Log, error) {
	l.connection.Save(&log)
	return log, nil
}

// UpdateLog implements LogRepository
func (l *logRepo) UpdateLog(log models.Log) (models.Log, error) {
	l.connection.Save(&log)
	l.connection.Find(&log)
	return log, nil
}

func NewLogRepository(connection *gorm.DB) LogRepository {
	return &logRepo{connection: connection}
}
func (l *logRepo) All() ([]models.Log, error) {
	logs := []models.Log{}
	l.connection.Find(&logs)
	return logs, nil

}
