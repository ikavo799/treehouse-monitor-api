package models

import "time"

type Log struct {
	ID            uint64    `gorm:"primary_key:auto_increment" json:"id"`
	HashID        string    `gorm:"type:varchar(255)" json:"title"`
	Level         string    `gorm:"type:varchar(255)" json:"level"`
	LastTimeAlert time.Time `json:"last_time_alert"`
	Caller        string    `gorm:"type:varchar(255)" json:"caller"`
	Message       string    `gorm:"type:varchar(255)" json:"msg"`
	Service       string    `gorm:"type:varchar(255)" json:"service"`
	KeyGroup      string    `gorm:"type:varchar(255)" json:"key_group"`
	Description   string    `gorm:"type:text" json:"description"`
	GroupID       uint64    `gorm:"not null" json:"-"`
	Group         Group     `gorm:"foreignkey:UserID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"group"`
	StatusID      uint64    `gorm:"not null" json:"-"`
	Status        Status    `gorm:"foreignkey:UserID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"status"`
	Count         int64     `gorm:"not null" json:"count"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
}
