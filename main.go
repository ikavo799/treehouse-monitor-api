package main

import (
	_ "github.com/go-sql-driver/mysql" // import your used driver
	"github.com/labstack/echo/v4"
	"github.com/treehouse-monitor/handler"
)

func main() {
	e := echo.New()
	groupLogs := e.Group("/api/log")
	groupLogs.GET("", handler.GetLogs)
	e.Logger.Fatal(e.Start(":8080"))
}
